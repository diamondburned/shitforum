package main

import (
	"sort"
	"time"
)

func createPost(title, author, content string) error {
	node, err := node.Begin(true)
	if err != nil {
		return err
	}

	defer node.Rollback()

	e := Entry{
		Title:   title,
		Author:  author,
		Content: content,
		Date:    time.Now(),
	}

	if err := node.Save(&e); err != nil {
		return err
	}

	return node.Commit()
}

func getPost(id int) (*Entry, error) {
	var entry Entry
	if err := node.One("ID", id, &entry); err != nil {
		return nil, err
	}

	return &entry, nil
}

func getPosts() ([]*Entry, error) {
	var entries []*Entry
	if err := node.All(&entries); err != nil {
		return nil, err
	}

	sort.Slice(entries, func(i, j int) bool {
		return entries[i].Date.Unix() < entries[j].Date.Unix()
	})

	return entries, nil
}
