package main

import (
	"html/template"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
)

type Entry struct {
	ID      int `storm:"id,increment"`
	Title   string
	Author  string
	Content string
	Date    time.Time
}

type Article struct {
	*Entry

	DisplayDate string
	Preview     string
}

func main() {
	defer db.Close()

	tmpl := template.Must(template.ParseFiles("index.html"))
	page := template.Must(template.ParseFiles("page.html"))

	r := chi.NewRouter()
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)
	r.Use(middleware.Timeout(5 * time.Second))

	r.Post("/submit", func(w http.ResponseWriter, r *http.Request) {
		err := createPost(
			r.FormValue("title"),
			r.FormValue("author"),
			r.FormValue("content"),
		)

		if err != nil {
			http.Error(w, err.Error(), 500)
			return
		}

		http.Redirect(w, r, "/", 302)
	})

	r.Get("/{ID}", func(w http.ResponseWriter, r *http.Request) {
		i, err := strconv.Atoi(chi.URLParam(r, "ID"))
		if err != nil {
			http.Error(w, err.Error(), 400)
			return
		}

		e, err := getPost(i)
		if err != nil {
			http.Error(w, err.Error(), 500)
			return
		}

		page.Execute(w, &Article{
			Entry:       e,
			DisplayDate: e.Date.Format("15:04, 02 Monday, 2006"),
		})
	})

	r.Get("/", func(w http.ResponseWriter, r *http.Request) {
		entries, err := getPosts()
		if err != nil {
			http.Error(w, err.Error(), 500)
			return
		}

		var articles = make([]*Article, len(entries))
		for i, e := range entries {
			articles[i] = &Article{
				Entry:       e,
				DisplayDate: e.Date.Format("15:04, 02 Monday, 2006"),
				Preview:     strings.Split(e.Content, "\n")[0] + "...",
			}
		}

		tmpl.Execute(w, articles)
	})

	http.ListenAndServe(":3000", r)
}
