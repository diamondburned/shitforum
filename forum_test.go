package main

import (
	"testing"

	"github.com/davecgh/go-spew/spew"
)

func TestForum(t *testing.T) {
	if e := createPost("tdeo gay", "diamond", "tdeo gay:\ntrue"); e != nil {
		t.Fatal(e)
	}

	posts, err := getPosts()
	if err != nil {
		t.Fatal(err)
	}

	if len(posts) == 0 {
		t.Fatal("No posts!")
	}

	p, err := getPost(posts[0].ID)
	if err != nil {
		t.Fatal(err)
	}

	spew.Dump(p)
}
