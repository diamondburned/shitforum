package main

import (
	"github.com/asdine/storm"
	"github.com/asdine/storm/codec/protobuf"
)

var (
	db   *storm.DB
	node storm.Node
)

func init() {
	s, err := storm.Open("my.db", storm.Codec(protobuf.Codec))
	if err != nil {
		panic(err)
	}

	db = s

	node = db.From("posts")
	node.Init(&([]Entry{}))
}
